/**
 * Created by Administrator on 2016/12/2.
 */
/*登录注册*/
$(function(){
    $(".sign-in-box li").click(
        function () {
           $(this).addClass("le-ac");
           $(this).siblings().removeClass("le-ac")
        }
    );
    $(".sign-in").click(
        function () {
            $(".sign-in-show").show();
            $(".register-show").hide()
        }
    );
    $(".register").click(
        function () {
            $(".sign-in-show").hide();
            $(".register-show").show()
        }
    );
    $(".forget-password").click(
        function () {
            $(".forget-password-show").show();
            $(".forget-password-hide").hide()
        }
    )
    }
);
/*账户管理支付状态*/
$(function() {
    $(".pay-state a").click(
        function () {
            $(this).addClass("pay-all");
            $(this).siblings().removeClass("pay-all")
        }
    )
}
);
/*我的信息全选*/
$(function() {
    $(".all-notice").change(
        function () {
            if ($(".all-notice").attr("checked", true)) {
                $(".single-notice").attr("checked", true);
            } else {
                $(".single-notice").attr("checked",false);
            }
        }
    )
    }
);
/*培训对象选择*/
$(function(){
        $(".training-object").click(function(){
          $(this).addClass("active") ;
          $(this).siblings().removeClass("active") ;
        });
    }
);