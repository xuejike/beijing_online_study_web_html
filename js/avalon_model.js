/**
 * Created by Administrator on 2016/11/11.
 */



// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


function dataStr(time){
    var time1 = new Date(time).Format("yyyy-MM-dd");
    return time1;
}

function dataStr2(time){

    var time2 = new Date(time).Format("yyyy-MM-dd hh:mm:ss");
    return time2;
}

function dataStr3(time){

    var time3 = new Date(time).Format("yyyy年MM月dd日");
    return time3;
}


function getValue(el){
    if(el == "" || el == undefined){
        return null;
    }
    return el;
}

var pageInfo = new Object();



function getPageInfo(pageCurrent,pageSize,total){
    pageInfo.pageNo = pageCurrent;
    pageInfo.pageSize = pageSize;
    pageInfo.total = total;
}



function pageTool(pageTool,pageObj,sendPostF,getQueryParamF){

    pageTool.empty();

    pageTool.Paging({pagesize:  pageObj.pageSize ,count:  pageObj.total,
        callback:function(page,size,count){
            api.bdlog(arguments);
            /*        alert('当前第 ' +page +'页,每页 '+size+'条,总页数：'+count+'页');*/
            getPageInfo(page,size,pageObj.total);

            sendPostF(getQueryParamF(pageInfo),false);
        }});
}


/**
 * 返回上一页不刷新
 */
function goBack(){
    history.back();
}





var noticeDetailInformVm=avalon.define({
    $id:"noticeDetailInform",
    data:[],
    timeStr:function (time) {
        return dataStr(time);

    }
});
function getNoticeDetailInform() {
    var id=api.req.id;
    post("home/notice/getNoticeDetail.do",{id:id},function (data) {
        api.bdlog(data);
        noticeDetailInformVm.data=data;
    },function () {
        alert("数据加载失败")
    });
}


var basicDataTypeVm = avalon.define({
    $id:"basicDataType",
    dataJobType:[],
    dataQuasiOperatione:[],
    dataExam:[],
    dataUnitType:[],
    selectObj:"",
    selectJobType:"",
    selectQuasiOperatione:"",
    selectUnitType:"",
    selectExam:"",
    searchKey:"",
    beginTime:"",
    endTime:"",

    openTrainingPlan:function () {
        var url=
            urlBuild("./training-plan.html?selectObj={0}&selectJobType={1}&selectQuasiOperatione={2}&selectUnitType={3}&selectExam={4}&searchKey={5}"
        ,[this.selectObj,this.selectJobType,this.selectQuasiOperatione,this.selectUnitType,this.selectExam,this.searchKey]);
        window.location.href=url;
    },
    getObjByJobType:function (events) {
        var pid=events.target.value;
        //wanglu 2017-5-9 在默认不选择种类的情况下清空一下选择条件
        if(pid == ""){

/*            basicDataTypeVm.dataJobType = [];*/
            //把准操项目清空
            basicDataTypeVm.dataQuasiOperatione = [];
        }else{
            getQuasiOperationeByJobTypeBasicData(pid);
        }

        // wanglu 2017-5-9 注释
    /*if(events.target.value!=""&&events.target.value!=null&&events.target.value!=undefined)

        getQuasiOperationeByJobTypeBasicData(events.target.value);*/
    }
});

/**
 * 缓存基础数据
 */

function getQuasiOperationeBasicData(){
    postCache("home/basicData/getNameTypeByType.do",{type:"准操项目"},function (data) {
        api.bdlog(data);
        api.storage.set("dataQuasiOperatione",data);
        basicDataTypeVm.dataQuasiOperatione = data;

    },function (msg) {
        api.bdlog(msg);
    });
}

function getQuasiOperationeByJobTypeBasicData(pid){
    post("home/basicData/getQuasiOperationeByJobTypeBasicData.do",{pid:pid},function (data) {
        api.storage.set("dataQuasiOperatione",data);
        basicDataTypeVm.dataQuasiOperatione = data;

    },function (msg) {
        api.bdlog(msg);
    });
}



function getUnitTypeBasicData() {
    postCache("home/basicData/getNameTypeByType.do",{type:"单位类型"},function (data) {
        api.storage.set("unitBasicData",data);
        basicDataTypeVm.dataUnitType = data;

    },function (msg) {
        api.bdlog(msg);
    });
}


function getExamBasicData(){
    postCache("home/basicData/getNameTypeByType.do",{type:"考核性质"},function (data) {
        api.storage.set("examBasicData",data);
        basicDataTypeVm.dataExam = data;
    },function (msg) {
        api.bdlog(msg);
    });
}



function getJobTypeBasicData(){
    postCache("home/basicData/getNameTypeByType.do",{type:"作业类别"},function (data) {
        api.storage.set("jobTypeBasicData",data);
        basicDataTypeVm.dataJobType  = data;
    },function (msg) {
        api.bdlog(msg);
    });
}



function checkBasiceDataTime(){

    /*api.storage.clear();*/

    var basicDateTime = api.storage.get("basicDateTime");

    //如果取出的时间未定义，重新取一遍数据
    if(basicDateTime == undefined){
        addInitBasiceData();
        return;
    }

    initBasiceData();

}



//添加初始化数据
function addInitBasiceData(){
    //准操项目
    getQuasiOperationeBasicData();

    getUnitTypeBasicData();
    //获得考核性质数据
    getExamBasicData();
    //获得作业类别数据
    getJobTypeBasicData();


    //存取当前取数据的时间
    api.storage.set("basicDateTime",new Date().getTime());
}



//初始化加载基础数据
function initBasiceData(){
    basicDataTypeVm.dataJobType  = api.storage.get("jobTypeBasicData");
    basicDataTypeVm.dataExam = api.storage.get("examBasicData");
    basicDataTypeVm.dataUnitType  =api.storage.get("unitBasicData");
    basicDataTypeVm.dataQuasiOperatione  =api.storage.get("dataQuasiOperatione");
}





/**
 * 检测是否登陆单机事件，如果登陆跳转到相应的地址，如果没有登陆，跳转到登陆页面
 * @param url
 */
function checkLoginStatusClick(url){
    var loginTime = api.storage.get("homeLoginTime");

    //如果登陆时间未定义
    if(loginTime == undefined){

        //未登录
        api.storage.set("login_back_url",url);

        //跳转到登陆页面
        window.location.href = "/log-on.html";

        return;
    }

    window.location.href=url;

}


var loginStatusInfo = null;

function timeCheck(){

}



/**
 * 初始化检测登陆状态,返回boolean类型
 * @returns {boolean}
 */
function initCheckLoginStatus(){
    var loginTime = api.storage.get("homeLoginTime");
    //如果登陆时间未定义
    if(loginTime == undefined){
        return false;
    }

    return true;
}

/**
 * 保存用户信息
 * @param objData
 */
function saveLoginStatus(objData){
    //保存登陆用户信息
    api.storage.set("homeUserInfo",objData);
    api.storage.set("homeLoginTime",new Date().getTime());
}


function getLoginUser(callback){
    post("home/user/getLoginUserInform.do",{},function (data) {
        api.bdlog(data);
        //保存用户信息
        saveLoginStatus(data);
        callback();
    },function (msg) {
        api.bdlog(msg);
    },function () {

    });
}




//发送短信请求
function getSmsCode(mobile){
    post("sms/getSmsCode.do",{mobile:mobile},function (data) {
        api.bdlog(data);
    },function (msg) {
        api.bdlog(msg);
    });
}


//获取登录状态下 手机发送的短信验证码
function getLoginUserMobileSmsCode(){
    post("sms/getLoginUserMobileSmsCode.do",{},function (data) {
        api.bdlog(data);
    },function (msg) {
        api.bdlog(msg);
    });
}




//获取字符串长度（汉字算两个字符，字母数字算一个）
function getByteLen(val) {
    var len = 0;
    console.log(val.length);
    for (var i = 0; i < val.length; i++) {
        len += 1;
    }
    return len;
}
// 只要键盘一抬起就验证编辑框中的文字长度，最大字符长度可以根据需要设定
function checkLength(obj) {
    var maxChars = 300;//最多字符数
    var curr = maxChars - getByteLen(obj.value);
    if (curr > 0) {
        document.getElementById("checklen").innerHTML = curr.toString();
    } else {
        document.getElementById("checklen").innerHTML = '0';
    }
}

function clearLength(){
    $("#checklen").text("300");
}
function getSystemConfig() {
   /* if(api.storage.get('getSystemConfig')==null||api.storage.get('getSystemConfig')==''||api.storage.get('getSystemConfig')==undefined) {*/
        post("home/systemConfig/getSystemConfig.do", {}, function (data) {
            api.bdlog("systemconfig:" + data);
          /*  for(var i =0;i<data.length;i++){
                api.storage.set(data[i],data[i])
            }
*/
            api.storage.set("systemConfigData", data);
        /*    api.storage.set("idPhoto", data.idPhoto);
            api.storage.set("headPhoto", data.headPhoto);
            api.storage.set("teacherHeadPhoto", data.teacherHeadPhoto);
            api.storage.set("webName", data.webName);
            api.storage.set("messageAccount", data.messageAccount);
            api.storage.set("serviceTelephone", data.serviceTelephone);
            api.storage.set("address", data.address);
            api.storage.set("email", data.email);*/
            api.storage.set("getSystemConfig", true);
        }, function (data) {
            api.bdlog(data);
        });
 /*   }*/
}
getSystemConfig();

footerBaseDataVm = avalon.define(
    {
        $id: 'footerBase',
        data:[]
    }
);

function getFooterBaseData() {
    //获取data
    footerBaseDataVm.data =  api.storage.get("systemConfigData");
    api.bdlog("footerBaseDataVm.data:"+footerBaseDataVm.data);
}
getFooterBaseData()


